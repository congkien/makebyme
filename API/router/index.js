
const userRouter = require('../user/user')

class RouterIndex {
    constructor(app) {
        this.app = app;
    }

    registerRoutes() {
        this.app.use('/user', userRouter);
    }
}

module.exports = (app) => { return new RouterIndex(app) };
