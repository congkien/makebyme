const express = require('express')
const Router = express.Router();
const userHandler = require('../hanlder/userHanlder')
const sqlConnection = require('../db')
const resHelper = require('../security/resHelper')
const sequelize = require('../db.sequelize')
const { QueryTypes } = require('sequelize');
const uploadHandler = require('../upload/upload.handler')

//const { error } = require('node:console');


Router.get("/", async (req, res) => {
    try {
        let status = req.body.status
        const results =     await userHandler.getAllUser(status, req.query)    
        res.send(results)
    } catch (error) {
        throw error;
    }
})



Router.get("/:id", async (req, res) => {
    try {
        const results = await userHandler.getUserInfor(req.params.id)
        res.send(results[0])
    } catch (error) {
        throw error;
    }
})

Router.post("/create", async (req, res) => {
    try {
        let data = req.body;
        await sqlConnection.query("INSERT INTO User SET ?", [data], (err, rows, fields) => {
            if (err) {
                res.send({
                    message: "Can not insert value, please check it again"
                })
            } else {
                console.log("ok");
                res.send(rows)
            }
        })
        console.log(req.body)
    } catch (error) {
        resHelper.sendError(res, error);
    }
})

Router.put("/update", async (req, res) => {
    try {
        let data = req.body;
        const results = await userHandler.getUserInfor(data.id)
        if (!results[0]) {
            res.send({
                message: "User not found."
            })
        }
        if (results[0].email !== data.email) {
            res.send({
                message: "you dont have to update email."
            })
        } else {
            await sqlConnection.query(`UPDATE User SET ? WHERE id = ${data.id}`, [data], (err, rows, fields) => {
                if (err) {
                    throw err
                } else {
                    console.log("ok");
                    res.send(rows)
                }
            })
        }
    } catch (error) {
        resHelper.sendError(res, error);
    }
})


Router.post('/upload', async (req, res) => {
    try {
        const result = await uploadHandler.uploadImage(req.body)
        resHelper.sendResponse(res, result);
    } catch (error) {
        resHelper.sendError(res, error);
    }
})

module.exports = Router;