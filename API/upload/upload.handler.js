const upload = require('../upload/upload_file')
const configCommon = require('../config/development')

async function uploadImage(body) {
    try {
        await upload.uploadToDisk(configCommon.image_dir, body.name, body.extention)
        return `${configCommon.link_image}/${body.name}.${body.extention}`
    } catch (error) {
        throw error;     
    }
}

module.exports = {
    uploadImage
}
