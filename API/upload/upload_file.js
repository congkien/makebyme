const fs = require('fs')

const uploadToDisk = (dir, name, extension) => {
        return new Promise((resolve, reject) => {
            fs.writeFile(`${dir}/${name}.${extension}`, extension, 'base64', function(err) {
                if (err) {
                    reject(err);
                } else {
                    resolve()
                }
            });
        })
}

const removeFromDisk = async (dir) => {
    return new Promise((resolve, reject) => {
        fs.unlink(dir, (err) => {
            if (err) {
                reject(err.message);
            } else {
                resolve()
            }
        });
    })
}

module.exports = {
    uploadToDisk,
    removeFromDisk
}
