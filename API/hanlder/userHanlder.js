const sqlConnection = require('../db')
const sequelize = require('../db.sequelize')
const { QueryTypes } = require('sequelize');
const resHelper = require('../security/resHelper')


const getUserInfor = async (id) => {
    const a = await sequelize.query(
        `SELECT * FROM User WHERE id = ?`,
        {
            replacements: [id],
            type: QueryTypes.SELECT
        }
    );
    console.log(a);
    return a
}


const getAllUser = async (status, params) => {
    let limit = params.limit
    let page = params.page
    const listObj = await sequelize.query (
        `SELECT * FROM User `,
        {
            type: QueryTypes.SELECT
        }
    )
    const total_page = Math.ceil(listObj.length / limit);
    switch (status) {
        case 0:
            const objStatus = await sequelize.query(
                `SELECT * FROM User WHERE status = ${status} ORDER BY createdAt DESC LIMIT ${limit} OFFSET  ${page}`,
                {
                    replacements: [status],
                    type: QueryTypes.SELECT
                }
            );
            return {
                total_page,
                objStatus
            }
            break;
        case 1:
            const objStatus1 = await sequelize.query(
                `SELECT * FROM User WHERE status = ${status} ORDER BY createdAt DESC LIMIT ${limit} OFFSET  ${page}`,
                {
                    replacements: [status],
                    type: QueryTypes.SELECT
                }
            );
            return {
                objStatus1,
                total_page
            }
            break;
        default:
            break
    }
}

const createUser = async (data) => {
    sqlConnection.query(`INSERT INTO User (email, username, fullname, status, deleted) VALUES ("${data.email}","${data.username}", "${data.fullname}",${data.status},${data.deleted} )`, (err, rows, fields) => {
        if (err) {
            throw err
        } else {
            console.log("ok");
        }
    })
}
module.exports = {
    getUserInfor,
    createUser,
    getAllUser
}