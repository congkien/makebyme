//const bodyParser = require('body-parser');
const express = require('express')
const bodyParser = require('body-parser');
var app = express();
// app.use(bodyParser.json);

//const userRouter = require('../API/user')



const port = process.env.PORT || 3000;
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));

const routerIndex = require('../API/router/index')(app);
routerIndex.registerRoutes();
app.listen(port);
console.log("App is listening on port"+ port)


