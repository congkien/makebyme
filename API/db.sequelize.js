const Sequelize = require('sequelize');

const sequelize = new Sequelize('test_nguyencongkien', 'test_nguyencongkien', 'nguyencongkien', {
    host: '45.76.146.236',
    dialect: 'mysql',
    define: {
        charset: 'utf8',
        collate: 'utf8_general_ci'
    },
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
});

module.exports = sequelize